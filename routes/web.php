<?php

use App\Http\Controllers\PostsController;
use App\Models\Address;
use App\Models\Country;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\Post;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create',function (){

});
// Route::get('/insert',function (){
//     $user = User::findOrFail(1);
//
//     $address = new Address(['name'=>'8588, Mathbaria, Bangladesh ']);
//     $user->address()->save($address);
// });
//Route::get('/update',function (){
//    $address = Address::whereUserId(1)->first();
//    $address->name = "2345 Update torrent ,Canada";
//    $address->save();
//});
//Route::get('/read',function (){
//    $user = User::findOrFail(1);
//   return $user->address->name;
//});
//Route::get('/delete',function (){
//    $user = User::findOrFail(1);
//    $user->address()->delete();
//
//    return "data delete successfully";
//});

/*{

Route::get('contact',[PostsController::class,'contact']);
Route::get('post/{name}/{class}/{id}/',[PostsController::class,'show_post']);

Route::resource('test',PostsController::class);
Route::get('/insert',function (){
    DB::insert('insert into posts(title,description, is_admin) values(?,?,?)',['PHP with Laravel','Laravel is the best thing that has happened to PHP ','1']);

});
Route::get('/read',function (){
  $results =  DB::select('select * from posts  where id = ?', [1]);
  foreach ($results as $post){
        return $post->description;
    }

});
Route::get('/update',function (){
   $updated = DB::update('update posts set title = "Updated Title" where id=?',[1]);
   return $updated;
});
Route::get('delete',function (){
    $deleted = DB::delete('delete from posts where id = ?',[2]);
});
*/
//|--------------------------------------------------------------------------
//| ELOQUENT
//|--------------------------------------------------------------------------
//Route::get('/read',function (){
// $posts = Post::all();
// foreach ($posts as $post){return $post->title;}
//
//});

/*
Route::get('findwhere',function (){
    return Post::where('id' ,2)->orderBy('id','desc')->take(1)->get();
});

Route::get('basic-insert',function (){
    $post = new Post;
    $post->title = 'New eloquent';
    $post->description = 'Eloquent description';
    $post->save();
});
Route::get('create',function (){
    Post::create([
        'title'=>'the create method',
        'description'=> ' I Love php in laravel',

    ]);
});
Route::get('/update',function (){
   Post::where('id',7)->where('is_admin', 0)->update(['title'=>'new php title','description'=>'I love my instructor']);
});
//Route::get('delete',function (){
//    $post = Post::find(1);
//    $post->delete();
//    return 'Data delete Successfully';
//});
Route::get('delete',function (){
   Post::destroy([4,5]);

    return 'Data delete Successfully';
});
Route::get('soft-delete',function (){
Post::destroy(6);

});
//Route::get('/read-deleted-data',function (){
//    return Post::withTrashed()->where('id',6)->get();
//});
Route::get('/read-deleted-data',function (){
    return Post::onlyTrashed()->where('is_admin',0)->get();
});
Route::get('restore',function (){
   Post::withTrashed()->where('is_admin',0)->restore();
});
Route::get('/forcedelete',function (){
   Post::withTrashed()->where('is_admin',0)->forceDelete();
});
Route::get('user/{id}/post',function ($id){
  return  User::find(1)->posts;
});
Route::get('post/{id}/user',function ($id){
   return Post::find($id)->user->name;
});
Route::get('/posts',function (){
    $user = User::find(1);
    foreach ($user->posts as $post){

      echo  $post->title."<br/>";
    }
});
Route::get('/user/{id}/role',function ($id){
//   $user = User::find($id)->roles()->orderBy('id','desc')->get();
 return User::find($id)->roles()->orderBy('id','desc')->get();
//   foreach ($user->roles as $role){
//     echo  $role->name;
//   }
});
//Accessing the intermediate table /pivot
Route::get('user/{id}/pivot',function ($id){
$user = User::find($id);
  foreach ($user->roles as $role){
     echo $role->pivot->created_at;
  }
});

Route::get('/user/country',function (){
$country = Country::find(1);
foreach ($country->posts as $post){
return $post;
}
});

//Polymorphic  Relations

Route::get('photos', [Photo::class]);
*/
