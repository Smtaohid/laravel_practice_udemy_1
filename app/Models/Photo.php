<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @method static find(int $int)
 */
class Photo extends Model
{

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }


}
