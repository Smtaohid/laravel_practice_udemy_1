<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
//**

/**
 * @property mixed|string $description
 * @property mixed|string $title
 * @method static create(string[] $array)
 * @method static find()
 */
class Post extends Model
{
    use SoftDeletes;
//protected $table = 'posts';
//protected $primaryKey = 'post_id';
protected
 $dates = ['deleted_at'];
protected $fillable = [

    'title',
    'description',

];
public function user(): BelongsTo
{
    return $this->belongsTo(User::class);
       }
public  function photos(): MorphMany
{
    return $this->morphMany(Photo::class, 'imageable');
}


      }
