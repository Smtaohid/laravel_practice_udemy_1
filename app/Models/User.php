<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Post;
use App\Models\Role;

/**
 * @method static find(int $int)
 * @method static findOrFail(int $int)
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function posts()
//    {
//      return $this->hasOne(Post::class);
//    }
    public function posts(): HasMany
    {
      return  $this->hasMany(Post::class);
    }

    public function roles(): BelongsToMany
    {
        //to customize tables name and columns follow the format below
        //  return $this->belongsToMany(Role::class,'user_roles','user_id','role_id');
       return $this->belongsToMany(Role::class)->withPivot('created_at');
    }

    public  function photos(): MorphMany
    {
        return $this->morphMany(Photo::class, 'imageable');
    }

    public function  address(): HasOne
    {
        return $this->hasOne(Address::class);
    }

}
