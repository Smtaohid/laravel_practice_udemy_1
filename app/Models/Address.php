<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static whereUserId(int $int)
 */
class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];
}
